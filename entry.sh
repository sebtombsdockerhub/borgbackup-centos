#!/bin/sh

# Import the cron configuration file
/usr/bin/crontab /config/crontab.cfg

# (Re)start cron
/usr/sbin/service crond restart

# Start borg web
/usr/bin/borgweb
