# BorgBackup-CentOS

This project is to create a Rock-on for the [Rockstor](http://rockstor.com/) storage appliance which enables the use of the modern, de-duplicating, backup system [BorgBackup](https://borgbackup.readthedocs.io/en/stable/).

Initial work has been to generate a Docker container which uses the latest [CentOS](https://www.centos.org/) release and the current BorgBackup version as pulled in using Python PIP.

Next steps are to develop the Rock-on JSON and decide how to run the backups.

The tricky bit is that we need Docker to have access to the directories that need to be backed up. Rockstor has a mechanism to do this, through the *volume* object, but each object can only be assigned to one share. How do we know how many shares the user wants to back up?
We can't just use an *environment* object to hold a list of directories because Rockstor won't know to pass these in the docker command line.

For the time being, it looks like we need to use the (at some point to be deprecated) *options* object to pass /mnt2 to docker with -v, in conjunction with an *environment* object that lists the shares in /mnt2 we actually want to back up.

Another area that needs investigation is how to exclude visible snapshots from the backup. They don't take much space (because of de-duplication) but they do take an ever increasing length of time to process as they multiply, because Borg "sees" them as real directories.
The not-so-great solution at the moment is to insist that the snapshot name includes "rockstor-snapshot" - which is potentially a problem as the user may have existing snapshot schedules and (at least at the moment) you can't change the snapshot prefix in the Rockstor GUI. Perhaps we can ask the Rockstor team to always include something like this in a snapshot name?

Rather than re-inventing the wheel, the plan is to use  [BorgMatic](https://github.com/witten/borgmatic) as the backup script itself. The config file will make use of the environment variables from the Rock-on JSON.

Bandwidth limiting has been provided based on the thoughts from [here](https://github.com/borgbackup/borg/issues/661).

We may want to bring in [BorgWeb](http://borgweb.readthedocs.io/en/stable/) in future, as this could assist with providing a useful front-end.

The work is copyright [Sebastian Tombs Enterprises Ltd](https://sebtombs.com/) 2021 and others as noted in the [LICENSE](https://bitbucket.org/sebtombs/borgbackup-centos/src/master/LICENSE) file and is released under the conditions stated in the said [LICENSE](https://bitbucket.org/sebtombs/borgbackup-centos/src/master/LICENSE) file.

# Status

The project status is **early development** so no reliance should be made on the current container content. Please feel free, however, to use it for experimentation.

# Dependencies

The JSON file relies on Rockstor using /mnt2 for the location of shares. If this changes then the Rock-on will break.

The Rock-on *options* object is used to pass critical parameters to docker, but it is documented that this will be deprecated in future. When it is deprecated, the JSON file will need reworking.
