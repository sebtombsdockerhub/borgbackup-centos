#!/bin/bash
     ## -q, --quiet              do not output any transfer information at all
     ## -L, --rate-limit RATE    limit transfer to RATE bytes per second
#export RATE=307200		 define this in the environment passed to docker
pv -q -L $BORG_RATE  | "$@"
